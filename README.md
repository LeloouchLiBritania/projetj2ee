# TP J2EE

TP J2EE noté, TSI 2019-2020, ENSG

## Installation

Tout d'abord, il installer mysql s'il ne l'est pas déjà:

`sudo apt-get install mysql-server`

Ensuite, il faut lancer mysql et creer une nouvelle base de données et un nouvel utilisateur avec tout les droits sur cette base de données. 

```
sudo mysql
create database gestionEvenements;
create user 'user'@'%' identified by 'mysql';
grant all on gestionEvenements.* to 'user'@'%';
```


## Interface web

L'interface web est faite de pages de listage des entités (participants, évènements et intervenants), et de pages de gestion des entités, permettant d'ajouter des entités, ou d'en supprimer des existantes.
L'adresse de la page d'accueil est <http://localhost:8080>

